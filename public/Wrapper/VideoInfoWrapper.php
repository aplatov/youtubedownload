<?php

require '../header.php';

use App\YoutubeDl\Dto\YoutubeDlVideoInfo;
use App\YoutubeDl\Service\YoutubeDlService;

$url = $_POST["url"];
if (empty($url)) {
    throw new InvalidArgumentException('Url is empty', 422);
}

try {
    $service = new YoutubeDlService();
    /** @var YoutubeDlVideoInfo $result */
    $result = $service->getVideoInfo($url);
} catch (Exception $exception) {
    throw new RuntimeException('Video not found', 404);
}
?>

<h2><?= $result->getFullTitle(); ?></h2>

<!-- VIDEO PLAYER -->
<div class="col">
    <video id="videoplayer" name="videoplayer" class="video-js vjs-default-skin vjs-big-play-centered" controls preload="auto" width="720" height="460"
           poster="<?= $result->getThumbnail() ?>" data-setup="{}">

        <?php foreach ($result->getDownloadUrls()['video'] as $downloadUrl): ?>

            <source src="<?= $downloadUrl->getUrl() ?>" type='video/<?= $downloadUrl->getExtension() ?>' label="<?= $downloadUrl->getExtension() . '/' . $downloadUrl->getFormat() ?>">

        <?php endforeach; ?>
        <p class="vjs-no-js">
            To view this video please enable JavaScript, and consider upgrading to a web browser that
            <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
        </p>
    </video>
</div>

<div class="col" style="margin-top: 30px">
    <img class="img-responsive pull-left" style="margin: 10px" src="<?= $result->getThumbnail(); ?>" width="150px">
    <span><?= $result->getDescription(); ?></span>
</div>

<div class="col" style="margin-top: 10px">

    <ul>
        <?php foreach ($result->getDownloadUrls()['video'] as $downloadUrl): ?>
            <li>
                <span><?= $downloadUrl->getName(); ?></span>
                <span><?= $downloadUrl->getExtension(); ?></span>
                <span><a href="<?= $downloadUrl->getUrl(); ?>" target="_blank">
                    <?= sprintf('Download (%sMB)', $downloadUrl->getSize()); ?>
                </a>
            </span>
            </li>
        <?php endforeach; ?>
        <?php foreach ($result->getDownloadUrls()['videoOnly'] as $downloadUrl): ?>
            <li>
                <span><?= 'video only ' . $downloadUrl->getName(); ?></span>
                <span><?= $downloadUrl->getExtension(); ?></span>
                <span><a href="<?= $downloadUrl->getUrl(); ?>" target="_blank">
                    <?= sprintf('Download (%sMB)', $downloadUrl->getSize()); ?>
                </a>
            </span>
            </li>
        <?php endforeach; ?>
        <?php foreach ($result->getDownloadUrls()['audioOnly'] as $downloadUrl): ?>
            <li>
                <span><?= $downloadUrl->getName(); ?></span>
                <span><?= $downloadUrl->getExtension(); ?></span>
                <span><a href="<?= $downloadUrl->getUrl(); ?>" target="_blank">
                    <?= sprintf('Download (%sMB)', $downloadUrl->getSize()); ?>
                </a>
            </span>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
