var player,
    playerOptions = {
        controlBar: {
            children: [
                'playToggle',
                'progressControl',
                'volumePanel',
                'qualitySelector',
                'fullscreenToggle',
            ],
        },
    };

function showInfo() {
    var url = $("#youtubeUrl");
    if (!url.val()) {
        $("#notFoundDiv").show();
        $("#youtubeVideoInfo").hide();
        return;
    }

    $.ajax({
        type: 'POST',
        url: "Wrapper/VideoInfoWrapper.php",
        data: {
            'url': url.val()
        },
        success: function (html) {
            $("#notFoundDiv").hide();
            $infoDiv = $("#youtubeVideoInfo");
            $infoDiv.show();
            $infoDiv.html(html);

            if (player) {
                player.dispose();
            }

            player = videojs('videoplayer', playerOptions);
        },
        error: function () {
            $("#notFoundDiv").show();
            $("#youtubeVideoInfo").hide();
        }
    });
}