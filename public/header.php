<?php
require __DIR__ . '/../vendor/autoload.php';

/**
 * Error and Exception handling
 */
error_reporting(E_ALL);
set_error_handler('App\Error\Error::errorHandler');
set_exception_handler('App\Error\Error::exceptionHandler');