<?php

require 'header.php';

?>

<!DOCTYPE html>
<html lang="en-US">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <meta name="author" content="axmit">

    <title>
        Youtube DL
    </title>

    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link href="https://vjs.zencdn.net/7.1.0/video-js.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/silvermine-videojs-quality-selector@1.1.2/dist/css/quality-selector.css">

    <script type="text/javascript" src="js/youtubeDl.js"></script>
</head>

<body>
<div class="container">
    <div class="jumbotron text-center">
        <h1>Download from Youtube</h1>

        <div style="margin-top: 50px; margin-bottom: 50px">
            <div class="form-inline">
                <input id='youtubeUrl' type="url" class="form-control"
                       placeholder="Link to youtube video" style="width: 360px; margin-left: 10px"/>
                <button type="button" onClick="showInfo()" class="btn btn-primary">Show</button>
            </div>
        </div>
    </div>

    <div id="youtubeVideoInfo" class="col-sm-offset-1 col-sm-10" style="display: none; ">
    </div>

    <h2 id="notFoundDiv" class="col-sm-offset-1 col-sm-10" style="display: none;">
        Video not found
    </h2>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous">
</script>
<script type="text/javascript" src="js/plugins/video.js"></script>
<script src="https://unpkg.com/silvermine-videojs-quality-selector/dist/js/silvermine-videojs-quality-selector.min.js"></script>

</body>
</html>
