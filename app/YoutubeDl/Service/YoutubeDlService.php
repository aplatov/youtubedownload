<?php

namespace App\YoutubeDl\Service;

use App\YoutubeDl\Assembler\YoutubeDlVideoInfoAssembler;
use App\YoutubeDl\Dto\YoutubeDlVideoInfo;
use App\YoutubeDl\Exception\YoutubeDlException;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Class YoutubeDlService
 *
 * @package App\YoutubeDl\Service
 */
class YoutubeDlService
{
    /**
     * @var YoutubeDlVideoInfoAssembler
     */
    private $assembler;

    /**
     * YoutubeDlService constructor.
     */
    public function __construct()
    {
        $this->assembler = new YoutubeDlVideoInfoAssembler();
    }

    /**
     * @param string $url
     *
     * @return YoutubeDlVideoInfo
     * @throws YoutubeDlException
     */
    public function getVideoInfo(string $url)
    {
        if (filter_var($url, FILTER_VALIDATE_URL) === false) {
            throw YoutubeDlException::notValidUrl($url);
        }

        $process = new Process(['youtube-dl', '--dump-json', $url]);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $result = json_decode($process->getOutput(), true);

        return $this->assembler->assemble($result);
    }
}