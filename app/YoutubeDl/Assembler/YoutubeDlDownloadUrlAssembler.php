<?php

namespace App\YoutubeDl\Assembler;

use App\YoutubeDl\Dto\YoutubeDlDownloadUrl;

/**
 * Class YoutubeDlDownloadUrlAssembler
 *
 * @package App\YoutubeDl\Assembler
 */
class YoutubeDlDownloadUrlAssembler extends AbstractAssembler
{
    /**
     * @param array $data
     *
     * @return YoutubeDlDownloadUrl
     */
    public function assemble(array $data): YoutubeDlDownloadUrl
    {
        $dto = new YoutubeDlDownloadUrl();
        $dto->setName($this->getName($this->getValue($data, 'format')))
            ->setUrl($this->getValue($data, 'url'))
            ->setExtension($this->getValue($data, 'ext'))
            ->setFormat($this->getValue($data, 'format_note'))
            ->setSize($this->getSize($this->getValue($data, 'filesize')));

        return $dto;
    }

    /**
     * @param null|string $value
     *
     * @return null|string
     */
    private function getName(?string $value): ?string
    {
        if (empty($value)) {
            return null;
        }

        return trim(substr($value, strpos($value, '-') + 1));
    }

    /**
     * @param $value
     *
     * @return float
     */
    private function getSize($value): ?float
    {
        if (empty($value)) {
            return null;
        }

        return round(($value / (1024 * 1024)), 1);
    }
}