<?php

namespace App\YoutubeDl\Assembler;

use App\YoutubeDl\Dto\YoutubeDlVideoInfo;

/**
 * Class YoutubeDlVideoInfoAssembler
 *
 * @package App\YoutubeDl\Assembler
 */
class YoutubeDlVideoInfoAssembler extends AbstractAssembler
{
    /**
     * @var YoutubeDlDownloadUrlAssembler
     */
    private $urlAssembler;

    /**
     * YoutubeDlVideoInfoAssembler constructor.
     */
    public function __construct()
    {
        $this->urlAssembler = new YoutubeDlDownloadUrlAssembler();
    }

    /**
     * @param array $data
     *
     * @return YoutubeDlVideoInfo
     */
    public function assemble(array $data): YoutubeDlVideoInfo
    {
        $dto = new YoutubeDlVideoInfo();
        $dto->setFullTitle($this->getValue($data, 'fulltitle'))
            ->setDescription($this->getValue($data, 'description'))
            ->setThumbnail($this->getValue($data, 'thumbnail'));

        $urls['video']     = [];
        $urls['videoOnly'] = [];
        $urls['audioOnly'] = [];
        foreach ($this->getValue($data, 'formats') as $format) {
            $group = 'video';
            if ($this->getValue($format, 'vcodec') == 'none') {
                $group = 'audioOnly';
            }
            if ($this->getValue($format, 'acodec') == 'none') {
                $group = 'videoOnly';
            }

            $urls[$group][] = $this->urlAssembler->assemble($format);
        }
        $dto->setDownloadUrls($urls);

        return $dto;
    }
}