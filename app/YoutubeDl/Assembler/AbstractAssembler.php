<?php

namespace App\YoutubeDl\Assembler;

/**
 * Class AbstractAssembler
 *
 * @package App\YoutubeDl\Assembler
 */
class AbstractAssembler
{
    /**
     * @param array      $data
     * @param string     $name
     * @param mixed|null $default
     *
     * @return mixed|null
     */
    protected function getValue(array $data, string $name, $default = null)
    {
        return empty($data[$name]) ? $default : $data[$name];
    }
}