<?php

namespace App\YoutubeDl\Dto;

/**
 * Class YoutubeDlVideoInfo
 *
 * @package App\YoutubeDl\Dto
 */
class YoutubeDlVideoInfo
{
    /**
     * @var string
     */
    private $fullTitle;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $thumbnail;

    /**
     * @var array
     */
    private $downloadUrls;

    /**
     * @return string
     */
    public function getFullTitle(): ?string
    {
        return $this->fullTitle;
    }

    /**
     * @param string $fullTitle
     *
     * @return YoutubeDlVideoInfo
     */
    public function setFullTitle(?string $fullTitle)
    {
        $this->fullTitle = $fullTitle;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return YoutubeDlVideoInfo
     */
    public function setDescription(?string $description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    /**
     * @param string $thumbnail
     *
     * @return YoutubeDlVideoInfo
     */
    public function setThumbnail(?string $thumbnail)
    {
        $this->thumbnail = $thumbnail;
        return $this;
    }

    /**
     * @return array
     */
    public function getDownloadUrls(): array
    {
        return $this->downloadUrls;
    }

    /**
     * @param array $downloadUrls
     *
     * @return YoutubeDlVideoInfo
     */
    public function setDownloadUrls(array $downloadUrls)
    {
        $this->downloadUrls = $downloadUrls;
        return $this;
    }
}