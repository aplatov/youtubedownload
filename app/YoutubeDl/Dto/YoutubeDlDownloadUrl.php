<?php

namespace App\YoutubeDl\Dto;

/**
 * Class YoutubeDlDownloadUrl
 *
 * @package App\YoutubeDl\Dto
 */
class YoutubeDlDownloadUrl
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $extension;

    /**
     * @var float
     */
    private $size;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $format;

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return YoutubeDlDownloadUrl
     */
    public function setName(?string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getExtension(): ?string
    {
        return $this->extension;
    }

    /**
     * @param string $extension
     *
     * @return YoutubeDlDownloadUrl
     */
    public function setExtension(?string $extension)
    {
        $this->extension = $extension;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getSize(): ?float
    {
        return $this->size;
    }

    /**
     * @param float|null $size
     *
     * @return $this
     */
    public function setSize(?float $size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return YoutubeDlDownloadUrl
     */
    public function setUrl(?string $url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getFormat(): ?string
    {
        return $this->format;
    }

    /**
     * @param string $format
     *
     * @return YoutubeDlDownloadUrl
     */
    public function setFormat(?string $format): YoutubeDlDownloadUrl
    {
        $this->format = $format;
        return $this;
    }
}