<?php

namespace App\YoutubeDl\Exception;

use Exception;

/**
 * Class YoutubeDlException
 *
 * @package App\YoutubeDl\Exception
 */
class YoutubeDlException extends Exception
{
    /**
     * @param string $url
     *
     * @return YoutubeDlException
     */
    public static function notValidUrl(string $url): YoutubeDlException
    {
        return new static(sprintf('Url is not valid: %s', $url), 1);
    }
}